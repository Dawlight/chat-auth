"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = require("axios");
const querystring = require("querystring");
const lodash_1 = require("lodash");
class CouchDbClient {
    constructor(config = undefined) {
        this.config = config;
        this.get = (path, queryParams = {}) => __awaiter(this, void 0, void 0, function* () {
            var fullPath = this.getFullPath(path, queryParams);
            console.log(`GET: ${fullPath}`);
            try {
                const response = yield this.httpClient.get(fullPath);
                console.log(`${response.status} ${response.statusText}`);
                return response;
            }
            catch (error) {
                console.log(`${error.response.status} ${error.response.statusText}`);
                throw error;
            }
        });
        this.post = (path, data, queryParams = {}) => __awaiter(this, void 0, void 0, function* () {
            var fullPath = this.getFullPath(path, queryParams);
            console.log(`POST: ${fullPath}`);
            try {
                var response = yield this.httpClient.post(fullPath, data);
                console.error(`${response.status} ${response.statusText}`);
                return response;
            }
            catch (error) {
                console.log(`${error.response.status} ${error.response.statusText}`);
                throw error;
            }
        });
        this.put = (path, data, queryParams = {}) => __awaiter(this, void 0, void 0, function* () {
            var fullPath = this.getFullPath(path, queryParams);
            console.log(`PUT: ${fullPath}`);
            // console.log(JSON.stringify(data));
            try {
                var response = yield this.httpClient.put(fullPath, data);
                console.log(`${response.status} ${response.statusText}`);
                return response;
            }
            catch (error) {
                console.log(`${error.response.status} ${error.response.statusText}`);
                throw error;
            }
        });
        this.delete = (path, queryParams = {}) => __awaiter(this, void 0, void 0, function* () {
            var fullPath = this.getFullPath(path, queryParams);
            console.log(`DELETE: ${fullPath}`);
            try {
                var response = yield this.httpClient.delete(fullPath);
                console.error(`${response.status} ${response.statusText}`);
                return response;
            }
            catch (error) {
                console.log(`${error.response.status} ${error.response.statusText}`);
                throw error;
            }
        });
        this.getFullPath = (path, queryParams = {}) => {
            var quotifiedQueryParams = {};
            if (lodash_1.isEmpty(queryParams) === false) {
                quotifiedQueryParams = this.stringifyObjectProperties(queryParams);
                console.log('Not yet quotified: ', JSON.stringify(queryParams));
                console.log('Quotified: ', JSON.stringify(quotifiedQueryParams));
            }
            var queryString = querystring.stringify(quotifiedQueryParams);
            return queryString ? path + `?${queryString}` : path;
        };
        this.stringifyObjectProperties = (queryParams = {}) => {
            var quotifiedQueryParams = {};
            for (var prop in queryParams) {
                if (queryParams.hasOwnProperty(prop)) {
                    console.log(prop + ' is a property');
                    console.log('With a value of ', queryParams[prop]);
                    if (jsonProperties.findIndex((item) => item === prop) >= 0) {
                        console.log('Which is a string');
                        quotifiedQueryParams[prop] = JSON.stringify(queryParams[prop]);
                    }
                    else {
                        quotifiedQueryParams[prop] = queryParams[prop];
                    }
                }
            }
            return quotifiedQueryParams;
        };
        if (config) {
            this.config = config;
        }
        else {
            this.config = defaultConfig;
        }
        this.httpClient = axios_1.default.create({
            baseURL: `${this.config.protocol}://${this.config.auth.username}:${this.config.auth.password}@${this.config.host}:${this.config.port}/`,
            headers: {
                common: {
                    'Content-Type': 'application/json'
                }
            }
        });
    }
}
exports.CouchDbClient = CouchDbClient;
const jsonProperties = [
    'key',
    'keys',
    'startkey',
    'start_key',
    'endkey',
    'end_key'
];
const defaultConfig = {
    host: 'localhost',
    protocol: 'http',
    port: 5984,
    auth: {
        username: 'dawlight',
        password: 'Gubbeliner123'
    }
};
//# sourceMappingURL=couch-db-client.js.map