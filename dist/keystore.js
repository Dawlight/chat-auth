'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const path = require("path");
const { createKeyStore } = require('oidc-provider');
const keystore = createKeyStore();
Promise.all([
    keystore.generate('RSA', 2048, {
        alg: 'RS256',
        use: 'sig'
    })
]).then(() => {
    fs.writeFileSync(path.resolve('src/keystore.json'), JSON.stringify(keystore.toJSON(true), null, 2));
});
//# sourceMappingURL=keystore.js.map