"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const NodeCouchDb = require("node-couchdb");
const couch_db_client_1 = require("../couch-db/couch-db-client");
class CouchDbAdapter {
    constructor(name) {
        this.name = name;
        this.dbName = 'oidc_models';
        this.upsert = (id, payload, expiresIn) => __awaiter(this, void 0, void 0, function* () {
            console.log('Upserting ' + this.name + ' ' + id);
            try {
                var response = yield this.db.get(`${this.dbName}/${id}`);
                return yield this.update(id, payload, expiresIn, response.data);
            }
            catch (error) {
                if (error.response.status === 404)
                    return yield this.insert(id, payload, expiresIn);
            }
        });
        this.update = (id, payload, expiresIn, currentModel) => __awaiter(this, void 0, void 0, function* () {
            console.log('Updating ' + this.name + ' ' + id);
            return yield this.db.put(`${this.dbName}/${id}`, Object.assign(currentModel, payload));
        });
        this.insert = (id, payload, expiresIn) => __awaiter(this, void 0, void 0, function* () {
            console.log('Inserting ' + this.name);
            payload._id = id;
            payload.type = this.name;
            return yield this.db.put(`${this.dbName}/${id}`, payload);
        });
        this.find = (id) => __awaiter(this, void 0, void 0, function* () {
            console.log('Finding ' + this.name + ' ' + id);
            try {
                var response = yield this.db.get(`${this.dbName}/${id}`);
                // console.log('Response: ', JSON.stringify(response.data));
                return response.data;
            }
            catch (error) {
                return false;
            }
        });
        this.consume = (id) => __awaiter(this, void 0, void 0, function* () {
            console.log('Consuming ' + this.name + ' ' + id);
            var getDocumentResponse = yield this.db.get(`${this.dbName}/${id}`);
            return yield this.db.put(`${this.dbName}/${id}`, Object.assign(getDocumentResponse.data, { consumed: true }));
        });
        this.destroy = (id) => __awaiter(this, void 0, void 0, function* () {
            console.log('Destroying ' + this.name + ' ' + id);
            try {
                var getDocumentResponse = yield this.db.get(`${this.dbName}/${id}`);
                var grantId = getDocumentResponse.data.grantId;
                if (grantId) {
                    var documentsByGrantId = yield this.db.get(`${this.dbName}/_design/adapter/_view/by_grant_id`, { include_docs: true, key: grantId });
                    var deletePromises = documentsByGrantId.data.rows.map(row => {
                        console.log(JSON.stringify(row));
                        return this.db.delete(`${this.dbName}/${row.id}`, { rev: row.doc._rev });
                    });
                    return yield Promise.all(deletePromises);
                }
                else {
                    return this.db.delete(`${this.dbName}/${getDocumentResponse.data._id}`, { rev: getDocumentResponse.data._rev });
                }
            }
            catch (error) {
                throw error;
            }
        });
        const couchConfig = {
            host: 'localhost',
            protocol: 'http',
            port: 5984,
            auth: {
                user: 'dawlight',
                pass: 'Gubbeliner123'
            }
        };
        this.db = new NodeCouchDb(couchConfig);
        this.db = new couch_db_client_1.CouchDbClient({
            protocol: 'http',
            host: 'localhost',
            port: 5984,
            auth: {
                username: 'dawlight',
                password: 'Gubbeliner123'
            }
        });
    }
}
exports.CouchDbAdapter = CouchDbAdapter;
//# sourceMappingURL=couch-db-adapter.js.map