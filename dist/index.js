"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
process.env.DEBUG = 'oidc-provider:*';
const express = require("express");
const path = require("path");
const bodyParser = require("body-parser");
const Provider = require("oidc-provider");
const couch_db_adapter_1 = require("./adapters/couch-db-adapter");
const couch_db_client_1 = require("./couch-db/couch-db-client");
const { AdapterTest } = Provider;
const couchConfig = {
    host: 'localhost',
    protocol: 'http',
    port: 5984,
    auth: {
        username: 'dawlight',
        password: 'Gubbeliner123'
    }
};
const db = new couch_db_client_1.CouchDbClient(couchConfig);
const claims = {
    openid: ['sub']
};
function findById(context, id) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            console.log(`Find by id ${id}`);
            console.log(`==========================================================================================`);
            var user = yield db.get(`users/${id}`);
            console.log(`Found user ${JSON.stringify(user.data)}`);
            return {
                accountId: user.data._id,
                claims: function claims() {
                    return { sub: user.data._id };
                }
            };
        }
        catch (e) {
            console.log(`Some fucking error in the findById method!`);
            throw new Error(e);
        }
    });
}
const clients = [
    {
        client_id: 'chat',
        grant_types: ['implicit'],
        response_types: ['id_token token'],
        redirect_uris: ['https://example.com'],
        token_endpoint_auth_method: 'none'
    }
];
const features = {
    devInteractions: false,
    claimsParameter: true,
    clientCredentials: true,
    discovery: true,
    encryption: true,
    introspection: true,
    registration: true,
    request: true,
    requestUri: true,
    revocation: true,
    sessionManagement: true,
};
const keystore = require('./keystore.json');
const oidcConfiguration = {
    claims: claims,
    findById: findById,
    features: features,
    interactionUrl(context) {
        return `/interaction/${context.oidc.uuid}`;
    }
};
const oidc = new Provider('http://localhost:3000', oidcConfiguration);
const test = new AdapterTest(oidc);
oidc.initialize({ keystore, clients, adapter: couch_db_adapter_1.CouchDbAdapter })
    .then(() => {
    oidc.app.proxy = true;
})
    .then(() => {
    const app = express();
    app.set('trust proxy', true);
    app.set('view engine', 'ejs');
    app.set('views', path.resolve(__dirname, 'views'));
    const parse = bodyParser.urlencoded({ extended: false });
    app.get('/interaction/:grant', (request, response) => __awaiter(this, void 0, void 0, function* () {
        oidc.interactionDetails(request).then(details => {
            console.log('see what else is available to you for interaction views', details);
            const view = (() => {
                console.log(`Interaction reason: ${details.interaction.reason}`);
                switch (details.interaction.reason) {
                    case 'consent_prompt':
                    case 'client_not_authorized':
                        return 'interaction';
                    default:
                        return 'login';
                }
            })();
            response.render(view, { details });
        });
    }));
    app.post('/interaction/:grant/confirm', parse, (request, response) => {
        oidc.interactionFinished(request, response, {
            consent: {}
        });
    });
    app.post('/interaction/:grant/login', parse, (request, response, next) => {
        db.get(`users/_design/account/_view/by_username`, { key: request.body.username }).then((result) => {
            console.log(`Got the account! ${JSON.stringify(result.data, null, 4)}`);
            oidc.interactionFinished(request, response, {
                login: {
                    account: result.data.rows[0].id,
                    acr: '1',
                    remember: !!request.body.remember,
                    ts: Math.floor(Date.now() / 1000),
                },
                consent: {}
            });
        }).catch(next);
    });
    app.use('/', oidc.callback);
    app.listen(3000);
});
//# sourceMappingURL=index.js.map