import * as http from 'http';
import Axios, { AxiosInstance } from 'axios';
import * as querystring from 'querystring';
import { isEmpty } from 'lodash';

export class CouchDbClient {

  httpClient: AxiosInstance;
  constructor(private config: ICouchDbClientConfig = undefined) {
    if (config) {
      this.config = config;
    } else {
      this.config = defaultConfig;
    }

    this.httpClient = Axios.create({
      baseURL: `${this.config.protocol}://${this.config.auth.username}:${this.config.auth.password}@${this.config.host}:${this.config.port}/`,
      headers: {
        common: {
          'Content-Type': 'application/json'
        }
      }
    })
  }


  public get = async <T = any>(path: string, queryParams: object = {}) => {
    var fullPath = this.getFullPath(path, queryParams);

    console.log(`GET: ${fullPath}`);

    try {
      const response = await this.httpClient.get<T>(fullPath);

      console.log(`${response.status} ${response.statusText}`);

      return response;
    } catch (error) {
      console.log(`${error.response.status} ${error.response.statusText}`);
      throw error;
    }
  }


  public post = async <T = any>(path: string, data: object, queryParams: object = {}) => {
    var fullPath = this.getFullPath(path, queryParams);

    console.log(`POST: ${fullPath}`);

    try {
      var response = await this.httpClient.post<T>(fullPath, data);

      console.error(`${response.status} ${response.statusText}`);

      return response;
    } catch (error) {
      console.log(`${error.response.status} ${error.response.statusText}`);
      throw error;
    }
  }

  public put = async <T = any>(path: string, data: object, queryParams: object = {}) => {
    var fullPath = this.getFullPath(path, queryParams);
    console.log(`PUT: ${fullPath}`);
    // console.log(JSON.stringify(data));

    try {
      var response = await this.httpClient.put<T>(fullPath, data);

      console.log(`${response.status} ${response.statusText}`);

      return response;
    } catch (error) {
      console.log(`${error.response.status} ${error.response.statusText}`);
      throw error;
    }
  }

  public delete = async (path: string, queryParams: object = {}) => {
    var fullPath = this.getFullPath(path, queryParams);

    console.log(`DELETE: ${fullPath}`);

    try {
      var response = await this.httpClient.delete(fullPath);

      console.error(`${response.status} ${response.statusText}`);

      return response;
    } catch (error) {
      console.log(`${error.response.status} ${error.response.statusText}`);
      throw error;
    }
  }

  private getFullPath = (path: string, queryParams: object = {}) => {
    var quotifiedQueryParams = {};

    if (isEmpty(queryParams) === false) {
      quotifiedQueryParams = this.stringifyObjectProperties(queryParams);

      console.log('Not yet quotified: ', JSON.stringify(queryParams));
      console.log('Quotified: ', JSON.stringify(quotifiedQueryParams));
    }

    var queryString = querystring.stringify(quotifiedQueryParams);
    return queryString ? path + `?${queryString}` : path;
  }

  private stringifyObjectProperties = (queryParams: object = {}) => {
    var quotifiedQueryParams = {};

    for (var prop in queryParams) {
      if (queryParams.hasOwnProperty(prop)) {
        console.log(prop + ' is a property');
        console.log('With a value of ', queryParams[prop]);
        if (jsonProperties.findIndex((item) => item === prop) >= 0) {
          console.log('Which is a string');
          quotifiedQueryParams[prop] = JSON.stringify(queryParams[prop]);
        } else {
          quotifiedQueryParams[prop] = queryParams[prop];
        }
      }
    }

    return quotifiedQueryParams;
  }
}

const jsonProperties = [
  'key',
  'keys',
  'startkey',
  'start_key',
  'endkey',
  'end_key'
];

export interface ICouchDbClientConfig {
  host: string;
  protocol: string;
  port: number;
  auth: {
    username: string;
    password: string;
  }
}

export interface ICouchDbQueryParameters {
  attachments: boolean;
  att_encoding_info: boolean;
  atts_since: string[];
  conflicts: boolean;
  deleted_conflicts: boolean;
  latest: boolean;
  local_seq: boolean;
  meta: boolean;
  open_revs: string[];
  rev: string;
  revs: boolean;
  revs_info: boolean;
}


const defaultConfig = {
  host: 'localhost',
  protocol: 'http',
  port: 5984,
  auth: {
    username: 'dawlight',
    password: 'Gubbeliner123'
  }
};