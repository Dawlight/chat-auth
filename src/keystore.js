'use strict';
exports.__esModule = true;
var fs = require("fs");
var path = require("path");
var createKeyStore = require('oidc-provider').createKeyStore;
var keystore = createKeyStore();
Promise.all([
    keystore.generate('oct', 2048, {
        alg: 'SHA256',
        use: 'sig'
    }),
    keystore.generate('RSA', 2048, {
        alg: 'RS256',
        use: 'sig'
    })
]).then(function () {
    fs.writeFileSync(path.resolve('src/keystore.json'), JSON.stringify(keystore.toJSON(true), null, 2));
});
