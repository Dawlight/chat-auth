import * as NodeCouchDb from 'node-couchdb';
import { CouchDbClient } from '../couch-db/couch-db-client';
import { snakeCase } from 'lodash';

export class CouchDbAdapter {
  private db: CouchDbClient;
  private dbName: string = 'oidc_models';

  constructor(private name: string) {
    const couchConfig = {
      host: 'localhost',
      protocol: 'http',
      port: 5984,
      auth: {
        user: 'dawlight',
        pass: 'Gubbeliner123'
      }
    };

    this.db = new NodeCouchDb(couchConfig);

    this.db = new CouchDbClient({
      protocol: 'http',
      host: 'localhost',
      port: 5984,
      auth: {
        username: 'dawlight',
        password: 'Gubbeliner123'
      }
    });
  }

  public upsert = async (id: string, payload: any, expiresIn: number) => {
    console.log('Upserting ' + this.name + ' ' + id);
    try {
      var response = await this.db.get(`${this.dbName}/${id}`);

      return await this.update(id, payload, expiresIn, response.data);
    } catch (error) {
      if (error.response.status === 404)
        return await this.insert(id, payload, expiresIn);
    }
  };

  private update = async (id: string, payload: any, expiresIn: number, currentModel: any) => {
    console.log('Updating ' + this.name + ' ' + id);
    return await this.db.put(`${this.dbName}/${id}`, Object.assign(currentModel, payload));
  };


  private insert = async (id: string, payload: any, expiresIn: number) => {
    console.log('Inserting ' + this.name);

    payload._id = id;
    payload.type = this.name;
    return await this.db.put(`${this.dbName}/${id}`, payload);
  };


  public find = async (id: string) => {
    console.log('Finding ' + this.name + ' ' + id);

    try {
      var response = await this.db.get(`${this.dbName}/${id}`);
      // console.log('Response: ', JSON.stringify(response.data));
      return response.data;
    } catch (error) {
      return false;
    }
  };

  public consume = async (id: string) => {
    console.log('Consuming ' + this.name + ' ' + id);
    var getDocumentResponse = await this.db.get(`${this.dbName}/${id}`);
    return await this.db.put(`${this.dbName}/${id}`, Object.assign(getDocumentResponse.data, { consumed: true }));
  };

  public destroy = async (id: string) => {
    console.log('Destroying ' + this.name + ' ' + id);
    try {
      var getDocumentResponse = await this.db.get(`${this.dbName}/${id}`);
      var grantId = getDocumentResponse.data.grantId;

      if (grantId) {
        var documentsByGrantId = await this.db.get(`${this.dbName}/_design/adapter/_view/by_grant_id`, { include_docs: true, key: grantId });

        var deletePromises = (documentsByGrantId.data.rows as any[]).map(row => {
          console.log(JSON.stringify(row));
          return this.db.delete(`${this.dbName}/${row.id}`, { rev: row.doc._rev });
        })

        return await Promise.all(deletePromises);
      } else {
        return this.db.delete(`${this.dbName}/${getDocumentResponse.data._id}`, { rev: getDocumentResponse.data._rev });
      }
    } catch (error) {
      throw error;
    }
  };
}