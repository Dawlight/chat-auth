'use strict';

import * as fs from 'fs';
import * as path from 'path';
const { createKeyStore } = require('oidc-provider');

const keystore = createKeyStore();

Promise.all([
  keystore.generate('RSA', 2048, {
    alg: 'RS256',
    use: 'sig'
  })
]).then(() => {
  fs.writeFileSync(path.resolve('src/keystore.json'), JSON.stringify(keystore.toJSON(true), null, 2));
});